#include <gazebo/common/Plugin.hh>
#include <ros/ros.h>
#include "/home/meggyesijanos/diffdrivebot_ws/src/gazebo_line_sensor_plugin/include/gazebo_line_sensor_plugin/gazebo_line_sensor_plugin.h"


#include "gazebo_plugins/gazebo_ros_camera.h"

#include <string>

#include <gazebo/sensors/Sensor.hh>
#include <gazebo/sensors/CameraSensor.hh>
#include <gazebo/sensors/SensorTypes.hh>

#include <sensor_msgs/PointCloud2.h>

namespace gazebo
{
    // Register this plugin with the simulator
    GZ_REGISTER_SENSOR_PLUGIN(GazeboRosLine)

    // Constructor
    GazeboRosLine::GazeboRosLine():_nh("line_sensor_plugin"), _fov(6), _range(10)
    {
        // Topic name: lineSensor
        // Message type: sensor_msgs::PointCloud2
        // Size of the message queue: 1 
        
         _sensorPublisher = _nh.advertise<sensor_msgs::PointCloud2>("lineSensor", 1);
         
    }

    // Destructor
    GazeboRosLine::~GazeboRosLine()
    {
        ROS_DEBUG_STREAM_NAMED("camera", "Unloaded");
    }

    void GazeboRosLine::Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf)
    {
        // Make sure the ROS node for Gazebo has already been initialized
        if(!ros::isInitialized())
        {
            ROS_FATAL_STREAM("A ROS node for Gazebo ha not been initalized, unbale to load plugin" << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package");
        }

    CameraPlugin::Load(_parent, _sdf);
        // Copying from CameraPlugin into GazeboRosCameraUtils
        this->parentSensor_ = this->parentSensor;
        this->width_ = this->width;
        this->height_ = this->height;
        this->depth_ = this->depth;
        this->format_ = this->format;
        this->camera_ = this->camera;

        GazeboRosCameraUtils::Load(_parent, _sdf);
    }

    // Update the controller
    void GazeboRosLine::OnNewFrame(const unsigned char* _image, unsigned int _width, unsigned int _height, unsigned int _depth, const std::string &_format)
    {
        static int seq = 0;

        this->sensor_update_time_ = this->parentSensor_->LastUpdateTime();
        if(!this->parentSensor->IsActive())
        {
            if((*this->image_connect_count_) > 0)
            // do this first so there's chance for sensor to run once after activated
            this->parentSensor->SetActive(true);
        }
        else
        {
            if((*this->image_connect_count_) > 0)
            {
                common::Time cur_time = this->world_->SimTime();
                if((cur_time - this->last_update_time_) >= this->update_period_)
                {
                    this->PutCameraData(_image, cur_time);
                    this->PublishCameraInfo(cur_time);
                    this->last_update_time_ = cur_time;

                    sensor_msgs::PointCloud2 msg;
                    msg.header.stamp = ros::Time::now();
                    msg.header.frame_id = "";
                    msg.header.seq = seq;
                    msg.height = 1;
                    msg.width = 16;
                    msg.fields[0].name = "LineSensorData";
                    msg.fields[0].offset = 0;
                    msg.fields[0].datatype = sensor_msgs::PointField::UINT8;
                    msg.fields[0].count = 16;
                    msg.is_bigendian = false;
                    msg.point_step = 1;
                    msg.row_step = 16;
                    msg.is_dense = true;
                    //for(int i = 0; i < 16; i++)
                    //{
                    //_image[i] = msg.data[i];
                    //}
                    // intensity = 0.299*R + 0.587*G + 0.114*B;
                    _sensorPublisher.publish(msg);
                    seq++;
                }
            }
                
            
        }
        
    }
}