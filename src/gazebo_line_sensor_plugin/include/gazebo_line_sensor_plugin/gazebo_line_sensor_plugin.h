#ifndef GAZEBO_ROS_LINE_SENSOR_HH
#define GAZEBO_ROS_LINE_SENSOR_HH

#include <string>

#include <gazebo/plugins/CameraPlugin.hh>
#include <gazebo_plugins/gazebo_ros_camera_utils.h>

namespace gazebo
{
    class GazeboRosLine : public CameraPlugin, GazeboRosCameraUtils
    {

        // Constructor
        public: GazeboRosLine();

        // Destructor
        public: ~GazeboRosLine();

        // Load the sensor plugin.
        // _sensor Pointer to the sensor that loaded this plugin
        // _sdf SDF element that describes the plugin
        public: void Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf);

        // Update the controller
        protected: virtual void OnNewFrame(const unsigned char* _image, unsigned int _width, unsigned int _height, unsigned int _depth, const std::string &_format);
   
        ros::NodeHandle _nh;
        ros::Publisher _sensorPublisher;

        double _fov;
        double _range;
    };                
}

#endif